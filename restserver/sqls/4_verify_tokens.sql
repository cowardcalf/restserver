-- Step 4
-- Create verify tokens table
-- 01/02/2016

CREATE TABLE verify_tokens
(
	ver_token_id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	token VARCHAR(255),
	create_time TIMESTAMP,
	expiry_time TIMESTAMP,
	PRIMARY KEY (ver_token_id),
	KEY token_idx_user_id (user_id),
	CONSTRAINT token_fk_user_id FOREIGN KEY (user_id) REFERENCES users (user_id)
);