-- Step 5
-- Create users account types
-- 19/02/2016

CREATE TABLE user_account_types
(
	u_ac_type_id INT NOT NULL,
	name VARCHAR(255) NOT NULL UNIQUE,
	PRIMARY KEY (u_ac_type_id)
);

INSERT INTO user_account_types(u_ac_type_id, name) VALUES (0, 'normal');
INSERT INTO user_account_types(u_ac_type_id, name) VALUES (1, 'facebook');
INSERT INTO user_account_types(u_ac_type_id, name) VALUES (2, 'google');