-- Step 2
-- Create user_roles table
-- 01/01/2016

CREATE TABLE user_roles (
  user_role_id INT NOT NULL AUTO_INCREMENT,
  user_id INT NOT NULL,
  role VARCHAR(45) NOT NULL,
  PRIMARY KEY (user_role_id),
  UNIQUE KEY uni_user_id_role (role,user_id),
  KEY roles_idx_user_id (user_id),
  CONSTRAINT roles_fk_user_id FOREIGN KEY (user_id) REFERENCES users (user_id)
  );