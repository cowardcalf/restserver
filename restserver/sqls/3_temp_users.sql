-- Step 3
-- Insert test users
-- 01/01/2016

INSERT INTO users(username, password, is_verified)
VALUES ('admin','123456', 1);
INSERT INTO users(username, password, is_verified)
VALUES ('alex','123', 1);

INSERT INTO user_roles (user_id, role)  
SELECT user_id, 'ROLE_USER' FROM users WHERE username = 'admin';
INSERT INTO user_roles (user_id, role)  
SELECT user_id, 'ROLE_ADMIN' FROM users WHERE username = 'admin';
INSERT INTO user_roles (user_id, role)  
SELECT user_id, 'ROLE_USER' FROM users WHERE username = 'alex';