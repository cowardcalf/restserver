-- Step 1
-- Create users able
-- 01/01/2016

CREATE TABLE users
(
	user_id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(255) NOT NULL UNIQUE,
	password VARCHAR(255),
	last_name VARCHAR(255),
	first_name VARCHAR(255),
	address VARCHAR(255),
	city VARCHAR(255),
	is_verified TINYINT(1) DEFAULT 0,
	account_type TINYINT(1) DEFAULT 0,
	PRIMARY KEY (user_id)
)