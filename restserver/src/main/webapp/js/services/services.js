'use strict';

// simple stub that could use a lot of work...
myApp.factory('RESTService',
    function ($http) {

        var urlBase = 'http://localhost:8080/restserver';

        this.getCustomers = function () {
            return $http.get(urlBase);
        };

        this.getCustomer = function (id) {
            return $http.get(urlBase + '/' + id);
        };

        this.insertCustomer = function (cust) {
            return $http.post(urlBase, cust);
        };

        this.updateCustomer = function (cust) {
            return $http.put(urlBase + '/' + cust.ID, cust)
        };

        this.deleteCustomer = function (id) {
            return $http.delete(urlBase + '/' + id);
        };

        this.getOrders = function (id) {
            return $http.get(urlBase + '/' + id + '/orders');
        };

        return {
            get:function (urlBase, callback) {
                return $http({method:'GET', url:urlBase}).
                success(function (data, status, headers, config) {
                    callback(data);
                    //console.log(data.json);
                }).
                error(function (data, status, headers, config) {
                    console.log("failed to retrieve data");
                });
            }
        };

        //var urlBase = '/api/customers';
       // var dataFactory = {};

        /*return {
            get:function (urlBase, callback) {
                return $http({method:'GET', url:urlBase}).
                    success(function (data, status, headers, config) {
                        callback(data);
                        //console.log(data.json);
                    }).
                    error(function (data, status, headers, config) {
                        console.log("failed to retrieve data");
                    });
            }
        };*/
    }
);


// simple auth service that can use a lot of work... 
myApp.factory('AuthService',
    function ($http) {
        var currentUser = null;
        var authorized = false;

        var urlBase = '/restserver/login';

        // initMaybe it wasn't meant to work for mpm?ial state says we haven't logged in or out yet...
        // this tells us we are in public browsing
        var initialState = true;

        return {
            initialState:function () {
                return initialState;
            },
            login:function (usrname, password) {
                currentUser = name;
//                var myobject = {'userName':usrname,'pass':password}
                
                var myobject = {'username':usrname,'password':password}
                
                Object.toparams = function ObjecttoParams(obj) {
                    var p = [];
                    for (var key in obj) {
                        p.push(key + '=' + encodeURIComponent(obj[key]));
                    }
                    return p.join('&');
                };


                $http({
                    url: urlBase,
                    method: "POST",
                    data: Object.toparams(myobject),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Basic dGVzdDp0ZXN0'}
                }).then(function(response) {

                    console.log( response );
                    // success
                    authorized = true;
                    initialState = false;

                },
                function(response) { // optional
                    // failed
                    authorized = false;
                    console.log("failed to login" + response );
                });

                //console.log("Logged in as " + name);
            },
            logout:function () {
                currentUser = null;
                authorized = false;
            },
            isLoggedIn:function () {
                return authorized;
            },
            currentUser:function () {
                return currentUser;
            },
            authorized:function () {
                return authorized;
            }
        };
    }
);