<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page session="false"%>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Login Page</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" href="apple-touch-icon.png">

<link rel="stylesheet" href="css/bootstrap.min.css">
<style>
body {
	padding-top: 50px;
	padding-bottom: 20px;
}
</style>
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/main.css">

<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
	<!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->




	<div class="container">
		<!-- Example row of columns -->

		<div class="row">

			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<a href="#" class="active" id="login-form-link">Login</a>
							</div>
							<div class="col-xs-6">
								<a href="#" id="register-form-link">Register</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">

								<h2>Login with Username and Password</h2>

								<c:if test="${not empty error}">
									<div class="error">${error}</div>
								</c:if>
								<c:if test="${not empty msg}">
									<div class="msg">${msg}</div>
								</c:if>

								<c:url value="/login" var="loginUrl" />
								<!-- login-form -->
								<form id="login-form" action="${loginUrl}" method="post"
									role="form"
									style="${pageType == 'register' ? 'display:none;':'display:block'}"
									name="loginForm">

									<div class="form-group">
										<input type="text" name="username" id="username" tabindex="1"
											class="form-control" placeholder="Username" value="">
									</div>

									<div class="form-group">
										<input type="password" name="password" id="password"
											tabindex="2" class="form-control" placeholder="Password">
									</div>

									<div class="form-group text-center">
										<input type="checkbox" tabindex="3" class="" name="remember"
											id="remember"> <label for="remember">
											Remember Me</label>
									</div>

									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit"
													tabindex="4" class="form-control btn btn-login"
													value="Log In">
											</div>
										</div>
									</div>

									<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
													<a href="#" tabindex="5" class="forgot-password">Forgot
														Password?</a>
												</div>
											</div>
										</div>
									</div>

									<input type="hidden" name="${_csrf.parameterName}"
										value="${_csrf.token}" />
								</form>

								<!-- register-form -->
								<c:url value="/register" var="registerURL" />
								<c:choose>
									<c:when test="${pageType == 'register'}">
										<c:set value="display:block;" var="formStyle" />
									</c:when>
									<c:otherwise>
										<c:set value="display:none;" var="formStyle" />
									</c:otherwise>
								</c:choose>
								<form:form id="register-form" role="form" style="${formStyle}"
									modelAttribute="user" method="POST" enctype="utf8"
									action="${registerURL}">
									<div class="form-group">
										<form:errors path="username" element="div" />
										<form:input type="text" name="username" id="username"
											tabindex="1" class="form-control" placeholder="Username"
											value="" path="username" />
									</div>
									<div class="form-group">
										<form:errors path="password" element="div" />
										<form:input type="password" name="password" id="password"
											tabindex="2" class="form-control" placeholder="Password"
											path="password" />
									</div>
									<div class="form-group">
										<form:errors path="confirmPassword" element="div" />
										<form:errors element="div" />
										<form:input type="password" name="confirmPassword"
											id="confirmPassword" tabindex="3" class="form-control"
											placeholder="Confirm Password" path="confirmPassword" />
									</div>
									<div class="form-group">
										<form:input type="text" name="firstName" id="firstName"
											tabindex="4" class="form-control" placeholder="First Name"
											value="" path="firstName" />
									</div>
									<div class="form-group">
										<form:input type="text" name="lastName" id="lastName"
											tabindex="5" class="form-control" placeholder="Last Name"
											value="" path="lastName" />
									</div>
									<div class="form-group">
										<form:input type="text" name="address" id="address"
											tabindex="6" class="form-control" placeholder="Address"
											value="" path="address" />
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit"
													id="register-submit" tabindex="7"
													class="form-control btn btn-register" value="Register Now">
											</div>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	<!-- /container -->

	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>
		window.jQuery
				|| document
						.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')
	</script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="js/main.js"></script>


</body>
</html>