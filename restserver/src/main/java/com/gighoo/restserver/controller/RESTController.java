package com.gighoo.restserver.controller;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.gighoo.restserver.model.JSONObj;
import com.gighoo.restserver.model.JsonErrorObj;
import com.gighoo.restserver.model.User;
import com.gighoo.restserver.model.UserDTO;
import com.gighoo.restserver.model.UserJDBCTemplate;

@RestController
public class RESTController {

    // @RequestMapping(value = "/greeting")
    // public JSONObj greeting(@RequestParam(value = "name", defaultValue =
    // "World") String name) {
    // JSONObj json = new JSONObj();
    // json.setName(name);
    // json.setMessage("Hello! " + name);
    // return json;
    // }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public Object register(@RequestBody User user, HttpServletRequest request) {
	//
	// return new JsonErrorObj("There is a test response.", 0);
	//
	user = createUserAccount(user);
	if (user != null) {
	    sendVerificationEmail(user, request);
	    return user;
	} else
	    return new JsonErrorObj("There is an error when register a new user. Please try again.", 0);
    }

    private boolean sendVerificationEmail(User user, HttpServletRequest request) {
	String recipientAddress = user.getUsername(); // username is the email
	String subject = "Registration Confirmation";
	String baseUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
		+ request.getContextPath();
	String confirmationUrl = baseUrl + "/registrationVerify?token=" + user.getVerifiedToken().getToken();
	// String message = messages.getMessage("message.regSucc", null,
	// event.getLocale());
	String message = "Please click the link below to verify your email address.";

	ApplicationContext context = new ClassPathXmlApplicationContext("/configs/spring-email.xml");

	JavaMailSenderImpl mailSender = (JavaMailSenderImpl) context.getBean("mailSender");

	MimeMessage mimeMsg = mailSender.createMimeMessage();

	try {
	    // use the true flag to indicate you need a multipart message
	    MimeMessageHelper helper = new MimeMessageHelper(mimeMsg, true);
	    helper.setTo(recipientAddress);
	    helper.setSubject(subject);
	    helper.setText("<html><body><div>" + message + "</div><br/><br/><a href='" + confirmationUrl + "'>" + confirmationUrl + "</a>",
		    true);
	    mailSender.send(mimeMsg);
	} catch (MessagingException me) {
	    me.printStackTrace();
	    return false;
	}

	// SimpleMailMessage email = new SimpleMailMessage();
	// email.setTo(recipientAddress);
	// email.setSubject(subject);
	// email.setText(message + confirmationUrl);
	return true;
    }

    private User createUserAccount(User user) {
	User registered = null;
	ApplicationContext context = new ClassPathXmlApplicationContext("/configs/spring-database.xml");

	UserJDBCTemplate userJDBCTemplate = (UserJDBCTemplate) context.getBean("userJDBCTemplate");

	try {
	    registered = userJDBCTemplate.create(user);
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    ((ConfigurableApplicationContext) context).close();
	}
	return registered;
    }

    @RequestMapping(value = "/greeting")
    public JSONObj greeting(@RequestBody JSONObj newObj) {
	// JSONObj json = new JSONObj();
	// json.setName(name);
	// json.setMessage("Hello! " + name);
	return newObj;
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public JSONObj test() {
	JSONObj json = new JSONObj();
	json.setId(2);
	json.setContent("Yeah!!!");
	return json;
    }

    private void getRequestURLs(HttpServletRequest request) {
	System.out.println("request.getScheme() " + request.getScheme());
	System.out.println("request.getServerName() " + request.getServerName());
	System.out.println("request.getServerPort() " + request.getServerPort());
	System.out.println("request.getContextPath() " + request.getContextPath());
	System.out.println("request.getRequestURL() " + request.getRequestURL());
    }
}
