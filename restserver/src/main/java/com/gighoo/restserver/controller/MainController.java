package com.gighoo.restserver.controller;

import javax.validation.Valid;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.gighoo.restserver.model.User;
import com.gighoo.restserver.model.UserDTO;
import com.gighoo.restserver.model.UserJDBCTemplate;

@Controller
public class MainController {

    @RequestMapping(value = { "/" })
    public String indexPage() {
	return "redirect:/index.html";
    }

    @RequestMapping(value = "/registrationVerify", method = RequestMethod.GET)
    public String registraionVerify(@RequestParam(value = "token") String token) {
	ApplicationContext context = new ClassPathXmlApplicationContext("/configs/spring-database.xml");
	UserJDBCTemplate userJDBCTemplate = (UserJDBCTemplate) context.getBean("userJDBCTemplate");
	if (userJDBCTemplate.verifyUser(token) != null)
	    // return verified page
	    return "redirect:/index.html";
	else
	    // Go to not verified page
	    return "redirect:/index.html";
    }

}