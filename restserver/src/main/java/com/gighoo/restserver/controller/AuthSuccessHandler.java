package com.gighoo.restserver.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

import com.gighoo.restserver.model.JSONObj;

public class AuthSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
    private RequestCache requestCache = new HttpSessionRequestCache();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
	    Authentication authentication) throws ServletException, IOException {
	
	ObjectMapper mapper = new ObjectMapper();
	JSONObj obj = new JSONObj();
	obj.setMessage("Login Success!");
	String jsonInString = mapper.writeValueAsString(obj);
	response.setContentType("application/json");
	response.setCharacterEncoding("UTF-8");
	response.getWriter().write(jsonInString);
	
	SavedRequest savedRequest = requestCache.getRequest(request, response);

	if (savedRequest == null) {
	    clearAuthenticationAttributes(request);
	    return;
	}
	String targetUrlParam = getTargetUrlParameter();
	if (isAlwaysUseDefaultTargetUrl()
		|| (targetUrlParam != null && StringUtils.hasText(request.getParameter(targetUrlParam)))) {
	    requestCache.removeRequest(request, response);
	    clearAuthenticationAttributes(request);
	    return;
	}

	clearAuthenticationAttributes(request);

	
	
	
    }

    public void setRequestCache(RequestCache requestCache) {
	this.requestCache = requestCache;
    }
}
