package com.gighoo.restserver.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import com.gighoo.restserver.model.JsonErrorObj;

public class AuthFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
	    AuthenticationException exception) throws  IOException, ServletException {
	
	ObjectMapper mapper = new ObjectMapper();
	JsonErrorObj obj = new JsonErrorObj();
	obj.setErrorMessage("Failed to login in.");
	String jsonInString = mapper.writeValueAsString(obj);
	response.setContentType("application/json");
	response.setCharacterEncoding("UTF-8");
	response.getWriter().write(jsonInString);
	
	response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
    
}
