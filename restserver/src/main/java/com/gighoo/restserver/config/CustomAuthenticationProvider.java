package com.gighoo.restserver.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.gighoo.restserver.model.User;
import com.gighoo.restserver.model.UserJDBCTemplate;

public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String name = authentication.getName();
        User user = (User)authentication.getCredentials();
	ApplicationContext context = new ClassPathXmlApplicationContext("/configs/spring-database.xml");
	UserJDBCTemplate userJDBCTemplate = (UserJDBCTemplate) context.getBean("userJDBCTemplate");
	Integer type = user.getAccountType();
	if(type == null){
	    type = User.USER_ACCOUNT_TYPE_NORMAL;
	    user.setAccountType(type);
	}
	if(type == User.USER_ACCOUNT_TYPE_NORMAL){
	    user = userJDBCTemplate.loginUser(name, user.getPassword());
	}else{
	    // validate on the third party
	    // try login
	    user = userJDBCTemplate.loginUser(user);
	}
	((ConfigurableApplicationContext)context).close();
        if (user != null) {
            List<GrantedAuthority> grantedAuths = new ArrayList<>();
            grantedAuths.add(new SimpleGrantedAuthority(user.getRole()));
            Authentication auth = new UsernamePasswordAuthenticationToken(name, user.getPassword(), grantedAuths);
            return auth;
        } else {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
	return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
