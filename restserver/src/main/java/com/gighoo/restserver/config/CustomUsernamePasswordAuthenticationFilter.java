package com.gighoo.restserver.config;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.gighoo.restserver.model.User;

public class CustomUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
	if (!request.getMethod().equals("POST")) {
	    throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
	}
	UsernamePasswordAuthenticationToken authRequest;
	// support json only at this stage
	if ("application/json".equals(request.getHeader("Content-Type"))) {
	    User loginUser = readJsonPayload(request);
	    authRequest = new UsernamePasswordAuthenticationToken(loginUser.getUsername(), loginUser);
	    return this.getAuthenticationManager().authenticate(authRequest);
	} else {
	    return super.attemptAuthentication(request, response);
	}

    }

    private static User readJsonPayload(HttpServletRequest request) {
	StringBuilder sb = new StringBuilder();
	BufferedReader reader = null;
	User user = null;
	try {
	    reader = request.getReader();
	    String line;
	    while ((line = reader.readLine()) != null) {
		sb.append(line).append('\n');
	    }
	    ObjectMapper mapper = new ObjectMapper();
	    user = mapper.readValue(sb.toString(), User.class);
	} catch (Exception e) {
	    e.printStackTrace();
	} finally {
	    try {
		reader.close();
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
	return user;
    }

    // @Override
    // protected String obtainPassword(HttpServletRequest request) {
    // String password = null;
    //
    // if ("application/json".equals(request.getHeader("Content-Type"))) {
    // password = this.jsonPassword;
    // } else {
    // password = super.obtainPassword(request);
    // }
    //
    // return password;
    // }
    //
    // @Override
    // protected String obtainUsername(HttpServletRequest request) {
    // String username = null;
    //
    // if ("application/json".equals(request.getHeader("Content-Type"))) {
    // username = this.jsonUsername;
    // } else {
    // username = super.obtainUsername(request);
    // }
    //
    // return username;
    // }

}
