package com.gighoo.restserver.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class UserMapper implements RowMapper<User> {
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
	User user = new User();
	user.setId(rs.getInt("user_id"));
	user.setUsername(rs.getString("username"));
	user.setPassword(rs.getString("password"));
	user.setFirstName(rs.getString("first_name"));
	user.setLastName(rs.getString("last_name"));
	user.setAddress(rs.getString("address"));
	return user;
    }
}