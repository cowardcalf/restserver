package com.gighoo.restserver.model;

public class JsonErrorObj {
    private int errorType = 0;
    private String errorMessage;
    
    public JsonErrorObj(){
	
    }
    
    public JsonErrorObj(String errorMessage, int errorType){
	this.errorMessage = errorMessage;
	this.errorType = errorType;
    }
    
    public int getErrorType() {
        return errorType;
    }
    public void setErrorType(int errorType) {
        this.errorType = errorType;
    }
    public String getErrorMessage() {
        return errorMessage;
    }
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
