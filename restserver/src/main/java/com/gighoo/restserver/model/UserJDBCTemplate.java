package com.gighoo.restserver.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

public class UserJDBCTemplate implements UserDAO {
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplateObject;

    @Override
    public void setDataSource(DataSource ds) {
	this.dataSource = ds;
	this.jdbcTemplateObject = new JdbcTemplate(ds);
    }

    public User create(User user) throws SQLException {
	Integer type = user.getAccountType();

	// insert into users table
	String insertSQL = "insert into users (username, password, first_name, last_Name, address, account_type, is_verified) values (?, ?, ?, ?, ?, ?, ?)";
	KeyHolder keyHolder = new GeneratedKeyHolder();

	jdbcTemplateObject.update(new PreparedStatementCreator() {
	    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(insertSQL, new String[] { "user_id" });
		ps.setString(1, user.getUsername());
		ps.setString(2, user.getPassword());
		ps.setString(3, user.getFirstName());
		ps.setString(4, user.getLastName());
		ps.setString(5, user.getAddress());

		if (type == null) {
		    ps.setInt(6, User.USER_ACCOUNT_TYPE_NORMAL);
		    ps.setBoolean(7, false);
		} else {
		    ps.setInt(6, type);
		    ps.setBoolean(7, true);
		}

		return ps;
	    }
	}, keyHolder);
	user.setId(keyHolder.getKey().intValue());

	// insert user role
	String roleSQL = "insert into user_roles (user_id, role) values (?,?)";
	jdbcTemplateObject.update(roleSQL, user.getId(), User.ROLE_USER);

	user.setRole(User.ROLE_USER);

	if (type == null || type == User.USER_ACCOUNT_TYPE_NORMAL)
	    // generate token
	    user.setVerifiedToken(generateVerifiedToken(user.getId()));

	return user;
    }

    private VerificationToken generateVerifiedToken(int userId) {
	VerificationToken newToken = new VerificationToken(userId);
	insertToken(newToken);
	return newToken;
    }

    private boolean insertToken(VerificationToken token) {
	// insert into users table
	String insertSQL = "insert into verify_tokens (user_id, token, create_time, expiry_time) values (?, ?, ?, ?)";
	KeyHolder keyHolder = new GeneratedKeyHolder();
	jdbcTemplateObject.update(new PreparedStatementCreator() {
	    public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(insertSQL, new String[] { "ver_token_id" });
		ps.setInt(1, token.getUserId());
		ps.setString(2, token.getToken());
		ps.setTimestamp(3, new Timestamp(token.getCreateTime().getTime()));
		ps.setTimestamp(4, new Timestamp(token.getExpiryTime().getTime()));
		return ps;
	    }
	}, keyHolder);
	token.setId(keyHolder.getKey().intValue());
	return true;
    }

    public User verifyUser(String token) {
	User user;
	String SQL = "select ver_token_id, user_id, expiry_time from verify_tokens where token=?";
	try {
	    user = jdbcTemplateObject.queryForObject(SQL, new Object[] { token }, new RowMapper<User>() {
		@Override
		public User mapRow(ResultSet rs, int rowNumber) {
		    User tempUser = new User();
		    try {
			tempUser.setId(rs.getInt("user_id"));
			VerificationToken vToken = new VerificationToken();
			vToken.setId(rs.getInt("ver_token_id"));
			vToken.setExpiryTime(new Date(rs.getTimestamp("expiry_time").getTime()));
			tempUser.setVerifiedToken(vToken);
		    } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		    }

		    return tempUser;
		}
	    });
	    // check expiry date
	    if (user.getVerifiedToken().getExpiryTime().compareTo(new Date()) > 0) {
		updateSingleField(user.getId(), "is_verified", "1");
		deleteToken(user.getVerifiedToken().getId());
		return user;
	    } else {
		return null;
	    }
	} catch (EmptyResultDataAccessException e) {
	    return null;
	}
    }

    @Override
    public User getUser(Integer id) {
	String SQL = "select * from users where id = ?";
	User user = jdbcTemplateObject.queryForObject(SQL, new Object[] { id }, new UserMapper());
	return user;
    }

    public User getUser(String username) {
	String SQL = "select * from users where username = ?";
	try {
	    return jdbcTemplateObject.queryForObject(SQL, new Object[] { username }, new UserMapper());
	} catch (EmptyResultDataAccessException e) {
	    return null;
	}
    }

    public User loginUser(String username, String password) {
	User user;
	String role;
	String SQL = "select role from users u inner join user_roles ur on u.user_id = ur.user_id where username = ? and password = ? and is_verified = 1";
	try {
	    role = jdbcTemplateObject.queryForObject(SQL, new Object[] { username, password }, String.class);
	    user = new User();
	    user.setUsername(username);
	    user.setPassword(password);
	    user.setRole(role);
	    return user;
	} catch (EmptyResultDataAccessException e) {
	    return null;
	}
    }

    public User loginUser(User user) {
	User insertedUser = null;
	if (user.getAccountType() != User.USER_ACCOUNT_TYPE_NORMAL) {
	    try {
		insertedUser = getUser(user.getUsername());
		if (insertedUser == null) { // new user
		    insertedUser = create(user);
		}
	    } catch (SQLException sqle) {
		// Existed user
		sqle.printStackTrace();
	    }
	    return insertedUser;
	} else {
	    return loginUser(user.getUsername(), user.getPassword());
	}
    }

    @Override
    public List<User> listUsers() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public void delete(Integer id) {
	// TODO Auto-generated method stub
    }

    public int deleteToken(Integer id) {
	String SQL = "delete from verify_tokens where ver_token_id = ?";
	int affected = 0;
	affected = jdbcTemplateObject.update(SQL, new Object[] { id });
	return affected;
    }

    @Override
    public void update(Integer id, User user) {
	// TODO Auto-generated method stub
    }

    /**
     * Update a single field of a user. String or integer values are accepted
     * 
     * @param userId
     * @param fieldName
     * @param value
     */
    public int updateSingleField(int userId, String fieldName, String value) {
	// user ? to set the fieldName may have parameter issue
	String SQL = "update users set " + fieldName + " = ? where user_id = ?";
	int affected = 0;
	affected = jdbcTemplateObject.update(SQL, new Object[] { value, userId });
	return affected;
    }
}
