package com.gighoo.restserver.model;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class VerificationToken {
    private static final int EXPIRATION = 60 * 24;

    private Integer id;
    private int userId;
    private String token;
    private Date createTime;
    private Date expiryTime;

    public VerificationToken(){
	
    }
    
    public VerificationToken(int userId) {
	this.userId = userId;
	generateDates();
	token = generateToken();
    }

    /**
     * Generate create time and expiry time
     */
    private void generateDates() {
	Calendar cal = Calendar.getInstance();
	Timestamp tsNow = new Timestamp(cal.getTime().getTime());
	createTime = new Date(tsNow.getTime());
	cal.setTime(tsNow);
	cal.add(Calendar.MINUTE, EXPIRATION);
	expiryTime = new Date(cal.getTime().getTime());
    }

    private String generateToken() {
	return UUID.randomUUID().toString();
    }

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public int getUserId() {
	return userId;
    }

    public void setUserId(int user_id) {
	this.userId = user_id;
    }

    public String getToken() {
	return token;
    }

    public void setToken(String token) {
	this.token = token;
    }

    public Date getCreateTime() {
	return createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    public Date getExpiryTime() {
	return expiryTime;
    }

    public void setExpiryTime(Date expiryTime) {
	this.expiryTime = expiryTime;
    }
}
