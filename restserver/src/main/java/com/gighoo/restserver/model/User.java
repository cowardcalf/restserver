package com.gighoo.restserver.model;

public class User {
    // Should match values in DB
    public static final int USER_ACCOUNT_TYPE_NORMAL = 0;
    public static final int USER_ACCOUNT_TYPE_FACEBOOK = 1;
    public static final int USER_ACCOUNT_TYPE_GOOGLE = 2;
    
    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    
    private Integer id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String address;
    private String role;
    private boolean verified = false;
    private VerificationToken verifiedToken;
    private Integer accountType;
    private String loginToken;
    
    public User (){
	
    }
    
    public User (UserDTO userDto){
	this.username = userDto.getUsername();
	this.password = userDto.getPassword();
	this.firstName = userDto.getFirstName();
	this.lastName = userDto.getLastName();
	this.address = userDto.getAddress();
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public VerificationToken getVerifiedToken() {
        return verifiedToken;
    }

    public void setVerifiedToken(VerificationToken verifiedToken) {
        this.verifiedToken = verifiedToken;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }
    
}
